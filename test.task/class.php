<?

use Bitrix\Main\Context;
use Bitrix\Main\Loader;

class ConversionCourses extends CBitrixComponent
{
    private $arCollection = [];
    private $url = 'https://sebekon.ru/api/v2/collection/';

    public function executeComponent()
    {
        global $USER;
        if ($USER->IsAdmin()) {
            $this->loadDependencies();
            $this->prepareData();
            $this->includeComponentTemplate();
        }
    }

    private function loadDependencies()
    {
        try {
            if (!Loader::includeModule('iblock')) die('module iblock is not installed');
        } catch (\Bitrix\Main\LoaderException $e) {
            die($e->getMessage());
        }
    }

    private function prepareData()
    {
        $postData = Context::getCurrent()->getRequest()->toArray();
        if (is_set($postData['UPDATE'])) {
            $this->setCollections();
            $this->setCollectionsDescriptions();
            $this->updateAllIBElements();
            $this->arResult['RESULT'] = 'Y';
        }
    }

    private function setCollections()
    {
        $arFilter = [
            'IBLOCK_ID' => $this->arParams['CATALOG_ID'],
            '!PROPERTY_' . $this->arParams['PROPERTY_CODE'] => false,
        ];
        $arGroupBy = ['PROPERTY_MATERIAL',];
        $obElements = CIBlockElement::GetList([], $arFilter, $arGroupBy);
        while ($element = $obElements->Fetch()) {
            $this->arCollection[]['NAME'] = $element['PROPERTY_MATERIAL_VALUE'];
        }
    }

    private function setCollectionsDescriptions()
    {
        $arCurls = [];
        $curlMulti = curl_multi_init();

        foreach ($this->arCollection as $id => $collection) {
            $arCurls[$id] = curl_init();
            $curlData = http_build_query(['collection' => $collection['NAME']]);
            $url = $this->url . '?' . $curlData;
            $options = [
                CURLOPT_URL => $url,
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
            ];
            curl_setopt_array($arCurls[$id], $options);
            curl_multi_add_handle($curlMulti, $arCurls[$id]);
        }

        $running = null;
        do {
            curl_multi_exec($curlMulti, $running);
        } while ($running > 0);

        foreach ($arCurls as $id => $curl) {
            $this->arCollection[$id]['DESCRIPTION'] = curl_multi_getcontent($curl);
            curl_multi_remove_handle($curlMulti, $curl);
        }
        curl_multi_close($curlMulti);
    }

    private function updateAllIBElements()
    {
        $ibElement = new CIBlockElement();
        $arSelect = ['ID', 'DETAIL_TEXT'];
        foreach ($this->arCollection as $collection) {
            if (!$collection['DESCRIPTION']) {
                continue;
            }
            $arFilter = [
                'IBLOCK_ID' => $this->arParams['CATALOG_ID'],
                'PROPERTY_' . $this->arParams['PROPERTY_CODE'] => $collection['NAME'],
                '!DETAIL_TEXT' => $collection['DESCRIPTION'],
            ];
            $obElements = CIBlockElement::GetList([], $arFilter, [], [], $arSelect);
            while ($element = $obElements->Fetch()) {
                $newFields = [
                    'DETAIL_TEXT' => $collection['DESCRIPTION'],
                ];
                $ibElement->Update($element['ID'], $newFields);
            }
        }
    }
}











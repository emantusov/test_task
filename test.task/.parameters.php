<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = [
	"GROUPS" => [
		"SETTINGS" => [
			"NAME" => 'Настройки'
		]
	],
	'PARAMETERS' => [
		"CATALOG_ID" => [
			"PARENT" => "SETTINGS",//можно сделать выпадающим списком
			"NAME" => "Ид каталога",
			"TYPE" => "STRING",
			"DEFAULT" => 2,
		],
        "PROPERTY_CODE" => [
            "PARENT" => "SETTINGS",//можно сделать выпадающим списком
            "NAME" => "Символьный код свойства",
            "TYPE" => "STRING",
            "DEFAULT" => 'COLLECTION',
        ],
	],
];